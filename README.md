# APP Tareas de horario laborales

## Descripción

Front en React para tareas diarias en jornada de 8 horas (jornada laboral) para ser ejecutado a través de docker, optimizado para hosts con  Linux o MacOs.


## Pre Requisitos

1. Tener instalado versión 19.0+ docker más informacion [link](https://docs.docker.com/engine/install/)
2. Tener instalado Docker-Compose 1.29.1 [link](https://docs.docker.com/compose/install/)
2. Contar con git instalado versión 2.20+
3. Tener instalado Makefile 3.0+
4. Clonar  Docker Compose desde el siguiente reporsitorio [link](https://gitlab.com/binter2021/compose)

## Instalación/Ejecución Local

Ingresar donde se clonó el docker compose

```bash
cd $TU_RUTA_REPO_DOCKER_COMPOSE
make project-run
```


