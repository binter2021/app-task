import React, { useState, useEffect } from 'react';
import { getTasks } from '../../utils/ApiUtils';
import  Card  from "../Card/Card";
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

export default function SearchResult(props) {
    const[summaryTasksWorkDay, setSummaryTasksWorkDay] = useState(null);
    function clear() {
        window.location.href = "/"
      }
      
      
    useEffect(() => {
        if (props.idToSearch.length > 0 ) {
            getTasks(props.idToSearch)
                .then(res => {
                    if(res && res.taskWorkDayList.length > 0){
                        setSummaryTasksWorkDay({
                            "tasksWorkDayList": res.taskWorkDayList
                        })

                    }
                })
        }
    
    }, [props.idToSearch])
    if(props.idToSearch){
        return(
            <Grid container  >
                <Grid container  >
                    <Button  variant="outlined" color="secondary"  onClick={clear}>
                        Limpiar Búsqueda
                     </Button>
                </Grid>

                {
                    summaryTasksWorkDay &&
                    summaryTasksWorkDay.tasksWorkDayList.map(function(tasksWorkDay,index) {
                        return <Grid item xs={3} key={index}><Card key={index} summaryTasksWorkDay={tasksWorkDay} /></Grid>
            
                    })
                } 
            </Grid>
        )
    }else{

        return(
            <div></div>
            )
    }
}