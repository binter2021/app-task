// import 'date-fns';
import React from 'react';
import TextField from '@material-ui/core/TextField';

export default function MaterialUIPickers() {
  // The first commit of Material-UI
  

  return (
    <TextField
    id="date"
    label="date"
    type="date"
    defaultValue="2020-01-02"
    // className={classes.textField}
    InputLabelProps={{
      shrink: true,
    }}
  />
  );
}
