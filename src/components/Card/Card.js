import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';


const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

export default function SimpleCard(props) {
  const classes = useStyles();
  
  const tasks = props.summaryTasksWorkDay.tasks;

  const strTaks = tasks.map((task,index) =>
    <li key={index}>{task}</li>
  );
  return (
  
    <Card className={classes.root}>
      <CardContent>
         <Typography variant="h5" component="p" >
              Nombre: {props.summaryTasksWorkDay.nameTask}
         </Typography> 
         <Typography variant="h5" component="p" >
         Duración: {props.summaryTasksWorkDay.duration}
        </Typography> 
        <Typography className={classes.pos} color="textSecondary">
          Identificador: {props.summaryTasksWorkDay.id}
        </Typography>

       <Typography variant="body2" component="p">
          Tarea(s): {strTaks}
          
        </Typography>
      </CardContent>
     
    </Card>

  );
}
