import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));
export default function SimpleSelect(props) {
  const [valueToSearch, setSelectToSearch] = React.useState('');

  const handleOnClick = () => {
    props.handleOnSearch(valueToSearch);
  };
  const classes = useStyles();
  const [indicators, setIndicators] = React.useState('');
  const handleChange = (event) => {
    setIndicators(event.target.value);
    setSelectToSearch(event.target.value)
  };
  
  return (
    <div>
      <FormControl className={classes.formControl}>
        <InputLabel id="demo-simple-select-label">Jornada:</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={indicators}
          onChange={handleChange}
        >
          <MenuItem value="todo">Jornada Laboral</MenuItem>
          {/*TODO agregar oppciones de busqueda por media jortanda o menos horas */}
        </Select>
        <br></br>
        <Button variant="outlined" color="primary" onClick={handleOnClick}>
          Buscar
        </Button>
      </FormControl>
      
    </div>
  );
}
