import axios from 'axios';

export const APP_ENV =  window._env_;

export const APIS_TASKS = APP_ENV.URL_APIS_TASKS;


export function getTasks(indicators_id, date) {
    return axios.get(
        `/api/v1/tasks`, {
            baseURL: APIS_TASKS
        }
    ).then(response => {
        return response.data;
    });
}