
import React from "react";

import {  Grid } from "@material-ui/core";

import './App.css';
// import moment from 'moment';
import SearchResult from "./components/SearchResult/SearchResult"
import SimpleSelect  from "./components/Selects/SimpleSelect";

function App() {

  const [indicatorsId, setSearchIndicators] = React.useState('');
  const [date, setSearchDates] = React.useState('');


  const handleOnSearch = (indicators,date) => {
    setSearchIndicators(indicators)
    setSearchDates(date)

  }
  return (
    <div>
    
    
      <Grid  container >
        <Grid item>
          <SimpleSelect handleOnSearch={handleOnSearch} /> 
        </Grid>
        


        <Grid item xs={12}>
          <SearchResult 
            idToSearch={indicatorsId}
            date={date}
          />
        </Grid>
      </Grid>
    </div >
  );
}

export default App;
